// On page load

$(document).ready(function() {
  let cookiesEnabled = localStorage.getItem('Cookies');
  if (cookiesEnabled === null ) {
    $(".cookie-notification").slideDown();
  }
  $(".owl-carousel").owlCarousel({
    autoplay: true,
    autoplayHoverPause: true,
    autoplayTimeout: 4500,
    dots: true,
    items: 1,
    loop: true,
  });
});

// Cookies

$(".accept-cookies").click(function() {
  localStorage.setItem('Cookies', 'Accepted');
  $(".cookie-notification").slideUp();
});

// Mobile menu slider

let mobileMenu = new Snap({
  element: document.getElementById('body-content'),
  disable: 'left',
  maxPosition: 266,
  minPosition: -266
});

mobileMenu.disable();

$(window).resize(function() {
  if (mobileMenu.state().state === "right" && $(window).width() >= 992) {
    mobileMenu.close();
  }
});

$(".mobile-menu").click(function() {
  if (mobileMenu.state().state === "right") {
    mobileMenu.close();
  } else {
    mobileMenu.open('right');
  }
});

// Mobile navigation

$(".mobile-nav li a").click(function(event) {
  let target = $(event.target);
  target.siblings("ul").slideDown();
});
